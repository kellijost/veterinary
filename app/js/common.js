// 'use strict';

(function () {
	// Custom JS
	console.log('Javascript is working!');
	console.log('JQuery ' + $.fn.jquery + " is working!"); //Activate the Slick Slider
  
	$(".meet_team__slider").slick({
	  // dots: true,
	  infinite: true,
	  slidesToShow: 3,
	  slidesToScroll: 1, 
	  nextArrow: '<i class="fas fa-chevron-right slick-arrow slider_right"></i>',
	  prevArrow: '<i class="fas fa-chevron-left slick-arrow slider_left"></i>',
	  dots: true
	});
  })();
  
  //Mobile Sticky Footer Scroll Capabilities
  (function () {
  const body = document.body;
  const nav = document.querySelector(".page-header nav");
  const menu = document.querySelector(".page-header .menu");
  const scrollUp = "scroll-up";
  const scrollDown = "scroll-down";
  const hideLogo = "hideLogo";
  const showLogo = "showLogo";
  let lastScroll = 0;
   
  window.addEventListener("scroll", () => {
	const currentScroll = window.pageYOffset;
	if (currentScroll == 0) {
	  body.classList.remove(scrollUp);
	  return;
	}

	if (currentScroll < 60){
		body.classList.remove(hideLogo);
	  body.classList.add(showLogo);
	}
	else{
	    body.classList.add(hideLogo);
	  body.classList.remove(showLogo);

	}
	 
	if (currentScroll > lastScroll && !body.classList.contains(scrollDown)) {
	  // down
	  body.classList.remove(scrollUp);
	  body.classList.add(scrollDown);
	} else if (currentScroll < lastScroll && body.classList.contains(scrollDown)) {
	  // up
	  body.classList.remove(scrollDown);
	  body.classList.add(scrollUp);
	}
	lastScroll = currentScroll;
  });
  })();

//Popper

$(function(){
	$('[data-toggle=popover]').popover({
		html: true,
		placement: "top",
		container: "body",
		content: function(){
			var content= $(this).attr("data-popover-content");
			return $(content).children(".hours_button__body").html();
		},
		offset: "-110, 20"
	});
  });

  $(function interiorScroll() {

	var $sidebar   = $(".interior_tile__navigation"), 
		$inner	   = $(".interior_tile__list"),
        $window    = $(window),
        offset     = $sidebar.offset(),
		topPadding = 15, 
		sidebarHeight = $sidebar.height(),
		innerHeight = $inner.height();
		
		if($sidebar.length > 0){
			if (window.innerWidth >= 992){	
				$window.scroll(function() {
				if ($window.scrollTop() > offset.top && $window.scrollTop() < sidebarHeight + offset.top - innerHeight) {
					$inner.stop().animate({
						marginTop: $window.scrollTop() - offset.top + topPadding
					});
				} else {
					$inner.stop().animate({
						marginTop: 0
					});
				}
				});
			}
			else {
				$inner.stop().animate({
					marginTop: 0
				});
			}
		}
});


    $(document).ready(function(){
        $("#myModal").modal('show');
    });
